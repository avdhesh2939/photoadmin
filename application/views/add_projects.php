
	
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Projects</li>
			</ol>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Projects</h1>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-8">
				<div class="panel panel-default">
					<div class="panel-heading">Add Projects</div>
					<?php echo $this->session->flashdata('success'); ?>	
					<div class="panel-body">
						<div class="col-md-8">
							<?php echo form_open_multipart("main/insert_project"); ?>
								
								<div id="questionForm">
								<div class="col-md-12">
								<div class="form-group">
								<label>Type</label>
								<select class="form-control" name="type" required>
									<option>Select</option>
									<option value="1">Portfolio</option>
									<option value="0">Black & White</option>
								</select><br>	
								</div>
								<label>Name</label>
								<div class="form-group">
								<input type="text" name="name" value="" id="name" class="form-control" required placeholder="Name">
								</div>
								<label>Cover Image</label>
								<div class="form-group">
								<input type="file" name="coverImage">
								</div>
								<div class="form-group">
									<label>Images</label>
									<input type="file" name="userfile[]" multiple="multiple">
								</div>
								</div>
								<div class="col-md-12 col-mt-10" style="margin-top: 10px;"><button type="submit" class="btn btn-primary">Submit</button></div>								


								
								</div>
						</form>

					</div>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
	</div><!--/.main-->

	<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>