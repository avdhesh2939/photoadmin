
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Sameer Photography</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->	
       
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="images/favicon.ico">
    </head>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
        <!--loader end-->
        <!-- main start  -->
        <div id="main">
            <!-- header start  -->
           <!-- header end -->
            <!-- wrapper  -->	
            <div id="wrapper">
                <!-- content -->	
                <div class="content">
                    <!--bottom-filter-wrap-->	
                    <div class="bottom-filter-wrap hor-filter-wrap">
                        <div class="scroll-down-wrap">
                            <div class="mousey">
                                <div class="scroller"></div>
                            </div>
                            <span>Scroll down to discover</span>
                        </div>
                        
                        <div class="count-folio round-counter">
                            <div class="num-album"></div>
                            <div class="all-album"></div>
                        </div>
                    </div>
                    <!--bottom-filter-wrap end-->	
                    <div class="ff_panel-conainer fl-wrap">
                        <!-- portfolio start -->
                        <div class="gallery-items min-pad   four-column fl-wrap lightgallery">
                            <!-- gallery-item-->
                             <?php foreach ($row as $rows) { ?>
                            <div class="gallery-item nature">
                                <div class="grid-item-holder hov_zoom">
                                    <a href='<?php echo "details/$rows->id" ?>'> <img  src="<?php echo base_url($rows->coverImage); ?>"    alt=""></a>
                                    <a href="<?php echo base_url($rows->coverImage); ?>" class="box-media-zoom   popup-image"><i class="fal fa-search"></i></a>                                    
                                    <div class="thumb-info">
                                        <h3><a href="portfolio-single.html"><?php echo $rows->name; ?></a></h3>
                                        
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                            <!-- gallery-item end-->
                            <!-- gallery-item-->
                           						
                        </div>
                        <!-- portfolio end -->
                    </div>
                </div>
                <!--content end-->	
                <!--share-wrapper-->
                <div class="share-wrapper">
                    <div class="share-container fl-wrap  isShare"></div>
                </div>
                <!--share-wrapper end-->
            </div>
            <!-- wrapper end -->
            <!-- sidebar -->
            <div class="sb-overlay"></div>
           <!-- sidebar end -->
            <!-- cursor-->
            <div class="element">
                <div class="element-item"></div>
            </div>
            <!-- cursor end-->          
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
    </body>
</html>