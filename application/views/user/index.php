
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Anand Photography</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->	
       
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="images/favicon.ico">
        <style>
        .fw_cb {
            font-size:35px;
        }
    </style>
    </head>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
        <!--loader end-->
        <!-- main start  -->
        <div id="main">
            <!-- header start  -->
                    <!-- header end -->
            <!-- wrapper  -->	
            <div id="wrapper">
                <!--content -->	
                <div class="content full-height  hidden-item no-mob-hidden">
                    <!-- fw-carousel-wrap -->
                    <div class="fw-carousel-wrap fsc-holder">
                        <!-- fw-carousel  -->
                        <div class="fw-carousel  fs-gallery-wrap fl-wrap full-height lightgallery" data-mousecontrol="true">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <!-- swiper-slide--> 
                                    <?php foreach ($row as $rows) { ?>
                                     
                                   
                                    <div class="swiper-slide hov_zoom">
                                        <img  src="<?php echo base_url($rows->coverImage); ?>"   alt="">
                                        <a href="<?php echo base_url($rows->coverImage); ?>" class="box-media-zoom   popup-image"><i class="fal fa-search"></i></a>
                                        <div class="thumb-info">
                                            <h3><a href="portfolio-single.html"><?php echo $rows->name; ?></a></h3>
                                            <p></p>
                                        </div>
                                        
                                    </div>
                                    <?php  } ?> 
                                    <!-- swiper-slide end-->  
                                    <!-- swiper-slide-->  
                                    
                                        <a href="portfolio.html" class="swiper-link"><span>View Portfolio</span></a>                                     
                                    </div>
                                    <!-- swiper-slide end-->                                     
                                </div>
                            </div>
                        </div>
                        <!-- fw-carousel end -->
                    </div>
                    <!--slider-counter-->
                    <div class="slider-counter_wrap">
                        <div class="fw-carousel-counter"></div>
                    </div>
                    <!--slider-counter end-->
                    <!--bottom-panel-->
                    <div class="bottom-panel">
                        <div class="bottom-panel-column bottom-panel-column_left">
                            <div class="scroll-down-wrap">
                                <div class="mousey">
                                    <div class="scroller"></div>
                                </div>
                                <span>Scroll down or  Swipe</span>
                            </div>
                            <div class="fs-controls_wrap">
                                <div class="fw_cb fw-carousel-button-prev"><i class="fal fa-angle-left"></i></div>
                                <div class="fw_cb fw-carousel-button-next"><i class="fal fa-angle-right"></i></div>
                            </div>
                        </div>
                        <div class="bottom-panel-column bottom-panel-column_right">
                            <div class="half-scrollbar">
                                <div class="hs_init"></div>
                            </div>
                        </div>
                    </div>
                    <!--bottom-panel end-->
                </div>
                <!--content end-->	
                <!--share-wrapper-->
                <div class="share-wrapper">
                    <div class="share-container fl-wrap  isShare"></div>
                </div>
                <!--share-wrapper end-->
            </div>
            <!-- wrapper end -->
            <!-- sidebar -->
            <div class="sb-overlay"></div>
            
            <!-- sidebar end -->
            <!-- cursor-->
            <div class="element">
                <div class="element-item"></div>
            </div>
            <!-- cursor end-->          
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
       
    </body>
</html>