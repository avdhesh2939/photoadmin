
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Sameer Photography</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->	
       
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="images/favicon.ico">
    </head>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
        <!--loader end-->
        <!-- main start  -->
        <div id="main">
            <!-- header start  -->
            <!-- header end -->
            <!-- wrapper  -->	
            <div id="wrapper">
                <!-- content -->	
                <div class="content">
                    <!-- column-image  -->	
                    <div class="column-image">
                        <div class="bg"  data-bg="<?php echo base_url('user_assets/images/bg/16.jpg');?>"></div>
                        <div class="overlay"></div>
                        <div class="column-title">
                            <h2>About Me</h2>
                            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar.</h3>
                        </div>
                        <div class="column-notifer">
                            <div class="scroll-down-wrap transparent_sdw">
                                <div class="mousey">
                                    <div class="scroller"></div>
                                </div>
                                <span>Scroll down  to Discover</span>
                            </div>
                        </div>
                        <div class="fixed-column-dec"></div>
                    </div>
                    <!-- column-image end  -->	
                    <!-- column-wrapper -->	
                    <div class="column-wrapper">
                        <div class="scroll-nav-wrap">
                            <nav class="scroll-nav scroll-init">
                                <ul>
                                   
                                </ul>
                            </nav>
                        </div>
                        <!--section  -->	
                        <section id="sec1">
                            <div class="container small-container">
                                <div class="section-title fl-wrap">
                                    <h3>My Little Story</h3>
                                    <h4>Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa</h4>
                                    <div class="section-number">01.</div>
                                </div>
                                <div class="column-wrapper_item fl-wrap">
                                    <div class="column-wrapper_text fl-wrap">
                                        <p>Cras mattis iudicium purus sit amet fermentum at nos hinc posthac, sitientis piros afros. Lorem ipsum dolor sit amet, consectetur adipisici elit, petierunt uti sibi concilium totius Galliae in diem sed eius mod tempor incidunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus et netus piros labore et dolore magna.
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.
                                        </p>
                                        <p>Praesent nec leo venenatis elit semper aliquet id ac enim. Maecenas nec mi leo. Etiam venenatis ut dui non hendrerit. Integer dictum, diam vitae blandit accumsan, dolor odio tempus arcu, vel ultrices nisi nibh vitae ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi varius lacinia vestibulum. Aliquam lobortis facilisis tellus, in facilisis ex vehicula ac. In malesuada quis turpis vel viverra.</p>
                                        
                                                                          </div>
                                </div>
                            </div>
                        </section>
                        <!--section end  -->	
                        
                        <!--footer end  -->	
                    </div>
                    <!-- column-wrapper -->	
                </div>
                <!--content end-->	
                <!--share-wrapper-->
                <div class="share-wrapper">
                    <div class="share-container fl-wrap  isShare"></div>
                </div>
                <!--share-wrapper end-->
            </div>
            <!-- wrapper end -->
            <!-- sidebar -->
            <div class="sb-overlay"></div>
           <!-- sidebar end -->
            <!-- cursor-->
            <div class="element">
                <div class="element-item"></div>
            </div>
            <!-- cursor end-->          
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
      
    </body>
</html>