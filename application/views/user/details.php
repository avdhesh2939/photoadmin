<?php 
$i=explode(',',$row['img']);
$count=count($i);

 ?>
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Sameer Tripathi</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============--> 
       
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="images/favicon.ico">
    </head>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
        <!--loader end-->
        <!-- main start  -->
        <div id="main">
            <!-- header start  -->
           <!-- header end -->
            <!-- wrapper  -->   
            <div id="wrapper">
                <!-- content -->    
                <div class="content">
                    <!-- column-image  -->  
                    <div class="column-image">
                        <div class="bg"  data-bg="<?php echo base_url($row['coverImage']); ?>"></div>
                        <div class="overlay"></div>
                        <div class="column-title">
                            <h2><?php echo $row['name']; ?></h2>
                            <h3></h3>
                        </div>
                        <div class="column-notifer">
                            <div class="scroll-down-wrap transparent_sdw">
                                <div class="mousey">
                                    <div class="scroller"></div>
                                </div>
                                <span>Scroll down  to Discover</span>
                            </div>
                        </div>
                        <div class="fixed-column-dec"></div>
                    </div>
                    <!-- column-image end  -->  
                    <!-- column-wrapper --> 
                    <div class="column-wrapper column-wrapper_smallpadding">
                        <!--fixed-bottom-content -->    
                        <div class="fixed-bottom-content fbc_white">
                            <div class="gallery-filters">
                                <a href="#" class="gallery-filter  gallery-filter-active" data-filter="*">All</a>
                                <a href="#" class="gallery-filter" data-filter=".nature">Nature</a>
                                <a href="#" class="gallery-filter" data-filter=".models">Models</a>
                                <a href="#" class="gallery-filter" data-filter=".couples">Couples</a>
                                <a href="#" class="gallery-filter" data-filter=".outdoor">Outdoor</a>
                            </div>
                        </div>
                        <!-- fixed-bottom-content end -->   
                        <!-- portfolio start -->
                        <div class="gallery-items min-pad   three-column fl-wrap lightgallery">
                            <!-- gallery-item-->
                             
                            
                                <?php foreach ($i as $key=>$value) { ?>
                                    <div class="gallery-item nature">

                                <div class="grid-item-holder hov_zoom">
                                    <img src="<?php echo base_url($value); ?>"    alt="">
                                    <a href="<?php echo base_url($value); ?>" class="box-media-zoom   popup-image"><i class="fal fa-search"></i></a>                                    
                                    <div class="thumb-info">
                                        <!-- <h3><a href="portfolio-single.html">Alone on Nature</a></h3>
                                        <p>Here you can place an optional description of your  Project</p>
                                    </div> -->
                                </div>
                            </div>
                             </div>
                        <?php } ?>
                            <!-- gallery-item end-->
                            <!-- gallery-item-->
                            
                            <!-- gallery-item end-->                            
                       
                        <!-- portfolio end -->
                    </div>
                    <!-- column-wrapper --> 
                </div>
                <!--content end-->  
                <!--share-wrapper-->
                <div class="share-wrapper">
                    <div class="share-container fl-wrap  isShare"></div>
                </div>
                <!--share-wrapper end-->
            </div>
            <!-- wrapper end -->
            <!-- sidebar -->
            <div class="sb-overlay"></div>
           <!-- sidebar end -->
            <!-- cursor-->
            <div class="element">
                <div class="element-item"></div>
            </div>
            <!-- cursor end-->          
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
       
    </body>
</html>