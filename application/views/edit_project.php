
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Projects</li>
			</ol>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Projects</h1>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-8">
				<div class="panel panel-default">
					<div class="panel-heading">Add Projects</div>
					<?php echo $this->session->flashdata('success'); ?>	
					<div class="panel-body">
						<div class="col-md-8">
							<?php echo form_open_multipart("bacadmin/main/update_project"); ?>
								<input type="hidden" name="id" value='<?php echo $row['pid'] ?>'>
								<div id="questionForm">
								<div class="col-md-12">
								<div class="form-group">
								<label>Type</label>
								<select class="form-control" name="type" required>
									<?php if($row['type']==1){ ?>
									<option value="1" selected>Completed</option>
									<option value="0">Work In Progress</option>
								<?php }else{ ?>
									<option value="1">Completed</option>
									<option value="0" selected>Work In Progress</option>
								<?php } ?>
								</select><br>	
								<label>Category Name</label>
								<select class="form-control" name='cat_id'>
									<?php foreach($cat as $r){ if($r->id==$row['cat_id']){ ?>

									<option value="<?php echo $r->id; ?>" selected><?php echo $r->cat_name; ?></option>
									<?php } else{ ?>
									<option value="<?php echo $r->id; ?>"><?php echo $r->cat_name; ?></option>	
									<?php } } ?>
								</select>
								</div>
								<label>Project Name</label>
								<div class="form-group">
								<input type="text" name="name" value="<?php echo $row['name']; ?>" id="name" class="form-control" required placeholder="Project Name">
								</div>
								<label>Description</label>
								<div class="form-group">
								<textarea name="description" value="" id="description" class="form-control" required placeholder="Description"><?php echo $row['description']; ?></textarea>
								</div>
								<label>Link</label>
								<div class="form-group">
								<input type="text" name="link" value="<?php echo $row['link']; ?>" id="link" class="form-control" required placeholder="Link">
								</div>
								</div>
								<div class="col-md-12 col-mt-10" style="margin-top: 10px;"><button type="submit" class="btn btn-primary">Submit</button></div>								


								
								</div>
						</form>

					</div>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
	</div><!--/.main-->

	