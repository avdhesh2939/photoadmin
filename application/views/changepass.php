
	
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Change Password</li>
			</ol>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Change Password</h1>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-8">
				<div class="panel panel-default">
					<div class="panel-heading">Change Password</div>
					<?php echo $this->session->flashdata('success'); ?>	
					<div class="panel-body">
						<div class="col-md-8">
							<?php echo form_open("bacadmin/main/confirmPass"); ?>
								
								<div id="questionForm">
								<div class="col-md-12">
								<label>New Password</label>
								<div class="form-group">
								<input type="text" name="password" value="" id="password" class="form-control" required placeholder="New Password">
								</div>
								<label>Confirm New Password</label>
								<div class="form-group">
								<input type="text" name="confirm_password" value="" id="confirm_password" class="form-control" required placeholder="Confirm New Password"><br>
								<span id='message'></span>
								</div>
								
								<div class="col-md-12 col-mt-10" style="margin-top: 10px;"><button type="submit" class="btn btn-primary">Submit</button></div>								
</div>
</div>
						</form>

					</div>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
	</div><!--/.main-->

  <script>
  	$('#password, #confirm_password').on('keyup', function () {
  if ($('#password').val() == $('#confirm_password').val()) {
    $('#message').html('Matching').css('color', 'green');
  } else 
    $('#message').html('Not Matching').css('color', 'red');
});</script>