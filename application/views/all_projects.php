
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	
	<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">All Projects</li>
			</ol>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">All Projects</h1>
			</div>
		</div>
<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">All Projects</div>
					<div class="panel-body">
						<?php echo $this->session->flashdata('delete'); ?>
						<?php echo $this->session->flashdata('update'); ?>
						<table data-toggle="table"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
						    <thead>
						    <tr>
						        <th data-align="right" >ID</th>
						        <th>Cover Image</th>
						        <th>Name</th>
						        <th>Type</th>
						        <th>Datetime</th>
						        <th>Action</th>

						    </tr>
						    </thead>
						    <tbody>
						    	<?php 
						    	$count=1;
						    	if(!empty($row)){
						    	foreach($row as $rows){ ?>
						    		<tr>
						    			<td><?php echo $count++; ?></td>
						    			<td><img src="<?php echo base_url($rows->coverImage); ?>" style="height:180px"></td>
						    			<td><?php echo $rows->name; ?></td>
						    			<td><?php if($rows->type==0){echo 'Black & White';}else{echo 'Portfolio';}; ?></td>
						    			<td><?php echo $rows->datetime; ?></td>
						    			<td><a href="<?php echo base_url("main/delete_project/$rows->id"); ?>"><button class="btn btn-danger">Delete</button></a></td>
						    		</tr>	
						    	<?php } } ?>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>
