<?php

class Loginmodel extends CI_Model {


	public function login_valid( $username, $password )
	{
		$q = $this->db->where(['username'=>$username,'password'=>$password])
						->get('user');

		if ( $q->num_rows() ) {
			return $q->row()->id;
		} else {
			return FALSE;
		}
	}

	
	public function confirm_password($arr){

			$q=$this->db->set('password', $arr)
					 	->where('id', 1)
					 	->update('user');
			if($q){

				 
				 return TRUE;
			}
			else{
				return FALSE;
			}
		}

	public function insert_project($arr){
		$date=new datetime();
		$c_date=$date->format('d-m-Y H:i:s');
		$arr['datetime']=$c_date;

		$q=$this->db->insert('projects',$arr);
		if($this->db->affected_rows()){
			return TRUE; 
		}else{
			return FALSE;
		}
	}

	public function get_projects(){
		$this->db->select('*');
		$this->db->from('projects as p');
		$q = $this->db->get();
		
		if($q->num_rows()){

			 $row=$q->result();
			 return $row;
		}
		else{
			return FALSE;
		}
	}

	public function delete_project($id){

		$q=$this->db->delete('projects',["id"=>$id]);
		return TRUE;
	
	}

	public function get_projectById($id){

		$this->db->select('*');
		$this->db->from('projects as p');
		$this->db->where('p.id',$id);
		$q = $this->db->get();
		if($q->num_rows()){

			 $row=$q->row_array();
			 return $row;
		}
		else{
			return FALSE;
		}
	}

	public function update_projectById($arr){

		$q=$this->db->set('type', $arr['type'])
					->set('cat_id', $arr['cat_id'])
					->set('name', $arr['name'])
					->set('description', $arr['description'])
					->set('link', $arr['link'])
				 	->where('id', $arr['id'])
				 	->update('projects');
		if($q){

			 
			 return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function get_Allprojects(){
		$this->db->select('*');
		$this->db->from('projects as p');
		$q = $this->db->get();
		
		if($q->num_rows()){

			 
			 return $q->num_rows();
		}
		else{
			return 0;
		}
	}

	public function get_Portprojects(){
		$this->db->select('*');
		$this->db->from('projects as p');
		$this->db->where('type','1');
		$q = $this->db->get();
		
		if($q->num_rows()){

			 return $q->num_rows();
		}
		else{
			return 0;
		}
	}

	public function get_Brojects(){
		$this->db->select('*');
		$this->db->from('projects as p');
		$this->db->where('type','0');
		$q = $this->db->get();
		
		if($q->num_rows()){

			 return $q->num_rows();
		}
		else{
			return 0;
		}
	}

}