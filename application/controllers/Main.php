<?php

class Main extends CI_Controller{

	public function __construct(){
				parent::__construct();
				if(!$this->session->userdata('id')){
					return redirect("login/index");
				}
				$this->load->model('loginmodel');
				$this->load->view('login_header');
				$this->load->view('navigation');


		}	
			
	public function add_projects(){


			$this->load->view('add_projects');
			$this->load->view('footer');



		}

	public function insert_project()
{       
	$row=$this->input->post();
//print_r($row);exit();
	$new_name1 = time()."-".$_FILES["coverImage"]['name'];
				$_FILES['file']['name']     = $new_name1;
                $_FILES['file']['type']     = $_FILES['coverImage']['type'];
                $_FILES['file']['tmp_name'] = $_FILES['coverImage']['tmp_name'];
                $_FILES['file']['error']     = $_FILES['coverImage']['error'];
                $_FILES['file']['size']     = $_FILES['coverImage']['size'];
                
                $uploadPath1 = 'uploads/coverImage/';
                $config['upload_path'] = $uploadPath1;
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('file')){
                   $row['coverImage']='uploads/coverImage/'.$this->upload->data('file_name');
    }

    $imgs = [];
    $files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {
				$new_name = time()."-".$_FILES["userfile"]['name'][$i];
				$_FILES['file']['name']     = $new_name;
                $_FILES['file']['type']     = $_FILES['userfile']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['userfile']['tmp_name'][$i];
                $_FILES['file']['error']     = $_FILES['userfile']['error'][$i];
                $_FILES['file']['size']     = $_FILES['userfile']['size'][$i];
                
                $uploadPath = 'uploads/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('file')){
                    array_push($imgs, 'uploads/'.$this->upload->data('file_name'));
    }
    
}

			$imge=implode(',',$imgs);
			$row['img']=$imge;
			
			$result=$this->loginmodel->insert_project($row);
			if($result){
						$this->session->set_flashdata('success','<div class="alert bg-success" role="alert">
											<svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>Your Project Successfully Inserted.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a></div>');

						}else{
						$this->session->set_flashdata('success','<div class="alert bg-danger" role="alert">
											<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>Insertion Failed.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
										</div>');	
						}
						return redirect('main/add_projects');

}

	public function get_projects(){


				$row=$this->loginmodel->get_projects();
				// print_r($row);
				// exit();
				$this->load->view('all_projects',['row'=>$row]);
				$this->load->view('footer');



			}

	public function delete_project($id){
				$row1=$this->loginmodel->get_projectById($id);
				$img=explode(',', $row1['img']);
				$img1=$row1['coverImage'];
				unlink($img1);
				//print_r($img);exit();
				$count=count($img);
				for($i=0;$i<$count;$i++){
					unlink($img[$i]);
				}
				$row=$this->loginmodel->delete_project($id);
				if($row){
					$this->session->set_flashdata('delete','<div class="alert bg-danger" role="alert">
									<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>Deleted Successfully.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
								</div>');
					return redirect("main/get_projects");
				}else{
					return redirect("main/get_projects");
				}
			}


	public function edit_project($id){


				$row=$this->loginmodel->get_projectById($id);
				$cat=$this->loginmodel->get_category();
				$this->load->view('edit_project',['row'=>$row,'cat'=>$cat]);
				$this->load->view('footer');



			}

	public function update_project(){

		$row=$this->input->post();
		$result=$this->loginmodel->update_projectById($row);
		if($result){
			$this->session->set_flashdata('update','<div class="alert bg-success" role="alert">
								<svg class="glyph stroked checkmark"><use xlink:href="#stroked-cancel"></use></svg>Updated Successfully.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
							</div>');
		}
		return redirect("main/get_projects");



	}

	

	public function changepass(){


				$this->load->view('changepass');
				$this->load->view('footer');



			}
	public function confirmPass(){
			$row=$this->input->post('password');
		
			$result=$this->loginmodel->confirm_password($row);
			if($result){
			$this->session->set_flashdata('success','<div class="alert bg-success" role="alert">
								<svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>Your Password Successfully Changed.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a></div>');

			}else{
			$this->session->set_flashdata('success','<div class="alert bg-danger" role="alert">
								<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Failed.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
							</div>');	
			}
			return redirect('main/changepass');



			}								

	
}
?>