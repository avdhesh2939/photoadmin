<?php

class User extends CI_Controller{


public function __construct(){

parent::__construct();
$this->load->view('user/header');
$this->load->view('user/footer');
$this->load->model('usermodel');

}

	
public function index(){


$row=$this->usermodel->get_projects();
$this->load->view('user/main',['row'=>$row]);




}

public function portfolio(){


$row=$this->usermodel->get_port_projects();
$this->load->view('user/portfolio',['row'=>$row]);

}


public function blachNwhite(){


$row=$this->usermodel->get_bNw_projects();
$this->load->view('user/blackNwhite',['row'=>$row]);

}

public function details($id){

$row=$this->usermodel->getProjectsById($id);
//print_r($row);
$this->load->view('user/details',['row'=>$row]);

}


public function about(){

$this->load->view('user/about');




}

public function contactQuery(){

$row=$this->input->post();

$this->load->library('email');

$this->email->from('your@example.com', 'Your Name');
$this->email->to('someone@example.com');
$this->email->cc('another@another-example.com');
$this->email->bcc('them@their-example.com');

$this->email->subject('Email Test');
$this->email->message('Testing the email class.');

$this->email->send();


$this->load->view('user/contacts');




}

public function contact(){

$this->load->view('user/contacts');




}




}

?>