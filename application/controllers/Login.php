<?php

Class Login extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('id')){
			return redirect('login/dashboard');
		}
		$this->load->helper('form');
		$this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer');
	}

public function user_login()
	{
	
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$this->load->model('loginmodel');
		$result=$this->loginmodel->login_valid($username,$password);
		if($result==1){
		$this->session->set_userdata('id',$result);	
		return redirect('login/dashboard');
		}
		else {
		$this->session->set_flashdata('login_failed','Invalid Username/Password.');
		return redirect('login');
			}
}

public function dashboard()
	{
		$this->load->model('loginmodel');
		$all['all']=$this->loginmodel->get_Allprojects();
		$all['portfolio']=$this->loginmodel->get_Portprojects();
		$all['black']=$this->loginmodel->get_Brojects();
		if(!$this->session->userdata('id')){
			return redirect('login');
		}
		$this->load->view('login_header');
		$this->load->view('navigation');
		$this->load->view('index',['row'=>$all]);
		$this->load->view('footer');
	}

public function logout()
	{
		
		$this->session->unset_userdata('id');
		return redirect('login');
	}	
	
}







